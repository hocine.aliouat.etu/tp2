import Component from '../component/Component';
import PizzaThumbnail from '../component/PizzaThumbnail';

export default class PizzaList extends Component {
	listPizza;

	constructor(listPizza) {
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			listPizza.map(pizza => new PizzaThumbnail(pizza))
		);
		this.listPizza = listPizza;
	}
}
