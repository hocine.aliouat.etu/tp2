import Component from './Component.js';
export default class Img extends Component {
	imgSrc;

	constructor(imgSrc) {
		super('img', { name: 'src', value: `${imgSrc}` });
	}
}
