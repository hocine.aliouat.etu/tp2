import Img from './component/Img.js';
import data from './data.js';
import Component from './component/Component.js';
import PizzaThumbnail from './component/PizzaThumbnail.js';
import PizzaList from './pages/PizzaList';

/*const title = new Component('h1', null, 'La carte');
document.querySelector('.pageTitle').innerHTML = title.render();*/

/*const img = new Component('img', {
	name: 'src',
	value:
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300',
});
document.querySelector('.pageContent').innerHTML = img.render();*/
/*const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);
document.querySelector('.pageContent').innerHTML = img.render();

const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();

const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);
document.querySelector('.pageContent').innerHTML = c.render();

const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector('.pageContent').innerHTML = pizzaThumbnail.render();*/

const pizzaList = new PizzaList(data);
document.querySelector('.pageContent').innerHTML = pizzaList.render();
